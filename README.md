# `ff-wordpress`

Gulp workflow for Wordpress theme development.

## Getting started

1. All the Wordpress system files are inside the `./site` folder;
2. All the theme files are inside the `./theme` folder;

## Building the theme

Open you terminal and run the command below:

```bash
cd ff-wordpress
npm run setup
```

It will install all the dependencies and generate the first build.

Once the setup is done, run:

```bash
npm run start
```

It will start the server and will be watching for changes. **Make sure that the project's directory matches the configuration on the `gulpfile.js`**

Although the `./theme` folder has it's own file architecture, everything will be in the right place after compiled.

## Running the theme

The build will be generated in the `./site/wp-content/themes/ff-wordpress` folder and no further action is needed for running the theme on Wordpress. Now you can go to `WP Admin > Appearence > Themes` and activate the theme.

_To avoid conflicts with the local environment, the `wp-config.php` it's being ignored._
