var gulp     = require('gulp')
    ,del     = require('del')
    ,sassdoc = require('sassdoc')
    ,merge   = require('merge-stream')
    ,gutil   = require('gulp-util')
    ,ftp     = require('vinyl-ftp')
    ,bs      = require('browser-sync').create()
    ,rs      = require('run-sequence')
    ,$       = require('gulp-load-plugins')();


var config = {
  themePath    : './site/wp-content/themes/ff-wordpress'
  ,stylesPath  : './theme/styles'
  ,scriptsPath : './theme/scripts'
  ,imagesPath  : './theme/images'
  ,vendorPath  : './theme/vendor'
  ,viewsPath   : './theme/views'
}

/*-----------------------------------*\
  @CLEAR
\*-----------------------------------*/
gulp.task('clear', function() {
  return del(
    config.themePath  + '/**/*'
    ,{force: true}
  );
});

/*-----------------------------------*\
  @STYLES
\*-----------------------------------*/
gulp.task('styles', function() {
  return gulp.src(config.stylesPath + '/**/*.scss')
    .pipe($.sass({outputStyle:'nested'})
      .on('error', $.notify.onError()))
    .pipe($.autoprefixer({
      browsers: ['last 2 version', '> 1%']
    }).on('error', $.notify.onError()))
    .pipe($.cssmin())
    .pipe($.rename({basename: 'style'}))
    .pipe(gulp.dest(config.themePath))
    .pipe($.size({title:'Styles'}))
    .pipe(bs.stream());
});

/*-----------------------------------*\
  @SASSDOC
\*-----------------------------------*/
gulp.task('sassdoc', function () {
  return gulp.src(config.stylesPath + '/**/*.scss')
    .pipe(sassdoc({dest: config.themePath + '/docs'})
      .on('error',$.notify.onError()))
    .pipe($.size({title:'SassDoc'}))
    .pipe(bs.stream());
});

/*-----------------------------------*\
  @SCRIPTS
\*-----------------------------------*/
gulp.task('scripts', function() {
  return gulp.src(config.scriptsPath + '/**/*.js')
    .pipe($.uglify()
      .on('error',$.notify.onError()))
    .pipe(gulp.dest(config.themePath))
    .pipe($.size({title: 'Scripts'}));
});

/*-----------------------------------*\
  @IMAGES
\*-----------------------------------*/
gulp.task('images', function() {
  return gulp.src(config.imagesPath + '/**/*')
    .pipe($.imagemin())
    .pipe(gulp.dest(config.themePath + '/images'))
    .pipe($.size({title: 'Images'}))
    .pipe(bs.stream());
});

/*-----------------------------------*\
  @VIEWS
\*-----------------------------------*/
gulp.task('views', function() {
  return gulp.src(config.viewsPath + '/**/*.php')
    .pipe(gulp.dest(config.themePath))
    .pipe(bs.stream());
});

/*-----------------------------------*\
  @UTILITY
\*-----------------------------------*/
gulp.task('util', function() {
  var img = gulp.src([
      config.themePath + '/images/screenshot.png'
      ,config.themePath + '/images/favicon.png'
    ]).pipe(gulp.dest(config.themePath));
  return merge(img);
});

/*-----------------------------------*\
  @BUILD
\*-----------------------------------*/
gulp.task('build', function(cb) {
  rs(
    'clear'
    ,'styles'
    ,'sassdoc'
    ,'scripts'
    ,'images'
    ,'views'
    ,'util'
    ,cb
  );
});

/*-----------------------------------*\
  @WATCH
\*-----------------------------------*/
gulp.task('watch', function() {
  gulp.watch(config.scriptsPath + '/**/*', ['scripts']);
  gulp.watch(config.stylesPath  + '/**/*', ['styles']);
  gulp.watch(config.imagesPath  + '/**/*', ['images']);
  gulp.watch(config.viewsPath   + '/**/*', ['views']);
});

/*-----------------------------------*\
  @SERVE
\*-----------------------------------*/
gulp.task('serve', function() {
  bs.init({
    proxy: 'localhost'
    ,port: 4000
  });
});

/*-----------------------------------*\
  @DEFAULT
\*-----------------------------------*/
gulp.task('default', function(cb) {
  rs(
    'build'
    ,'watch'
    ,'serve'
    ,cb
  );
});

/*-----------------------------------*\
  @FTP
\*-----------------------------------*/
gulp.task('ftp', function() {
  var conn = ftp.create({
    user: ''
    ,password: ''
    ,host: ''
    ,log: gutil.log
  });

  return gulp.src(config.themePath + '/**')
    .pipe(conn.newer('/ff'))
    .pipe(conn.dest('/ff'));
});

/*-----------------------------------*\
  @DEPLOY
\*-----------------------------------*/
gulp.task('deploy', function(cb) {
  rs(
    'build'
    ,'ftp'
    ,cb
  );
});
